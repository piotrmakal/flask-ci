import datetime

from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route("/")
    def hello_world():
        return f"Today is {datetime.date.today()}"

    return app


app = create_app()
