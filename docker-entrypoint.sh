#!/bin/sh
pip install gunicorn
NAME=${PWD##*/}
NUM_WORKERS=2
PROJECT=hello

exec gunicorn ${PROJECT}:app \
    --name $NAME \
    --workers $NUM_WORKERS \
