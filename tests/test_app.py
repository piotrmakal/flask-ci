from freezegun import freeze_time


@freeze_time("2012-01-14")
def test_should_return_today_date(client):
    response = client.get("/")
    assert response.data == b"Today is 2012-01-14"
