FROM python:3.8-alpine
ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

COPY requirements/* /app/requirements/
RUN pip install -r requirements/base.txt

COPY . /app

CMD ["sh", "docker-entrypoint.sh"]
